#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

typedef struct {
		cv::Mat buffer;
		cv::Point2d a, b, ponto, projecao;
} Dados;

void CallBack_Mouse(int event, int x, int y, int flags, void* userdata);
void DesenhaDados(Dados *dados);
void ProjetaPonto(Dados *dados);
void Flip(Dados *dados);
void Flip(cv::Point2d *p);

int main(int, char **) {
	Dados dados;
	dados.a = cv::Point2d(400, 200);
	dados.b = dados.ponto = dados.projecao = cv::Point2d(200, 200);
	dados.buffer = cv::Mat(600, 800, CV_8UC3);
	cv::namedWindow("Point-Line Projection", cv::WINDOW_AUTOSIZE | CV_GUI_NORMAL);
	cv::setMouseCallback("Point-Line Projection", CallBack_Mouse, &dados);
	DesenhaDados(&dados);
	cv::imshow("Point-Line Projection", dados.buffer);
	cv::waitKey();
	return 0;
}

void CallBack_Mouse(int event, int x, int y, int flags, void* userdata)	{
	Dados *dados = (Dados *)userdata;
	switch(event)	{
		case cv::EVENT_LBUTTONDOWN:
			dados->a = dados->b;
			dados->b = dados->ponto;
			ProjetaPonto(dados);
			break;
		case cv::EVENT_RBUTTONDOWN:
			break;
		case cv::EVENT_RBUTTONUP:
			break;
		case cv::EVENT_LBUTTONUP:
			break;
		case cv::EVENT_MOUSEMOVE:
			dados->ponto = cv::Point2d(x, y);
			ProjetaPonto(dados);
			break;
	}
	DesenhaDados(dados);
}

void DesenhaDados(Dados *dados) {
	std::stringstream notificacao_ss;
	notificacao_ss << "<" << dados->ponto.x << ", " << dados->ponto.y << ">";
	dados->buffer = cv::Scalar(0);
	cv::putText(dados->buffer, "Mouse: " + notificacao_ss.str(), cv::Point(20,580), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0, 0, 255), 1, cv::LINE_8, false);
	cv::line(dados->buffer, dados->projecao, dados->a, cv::Scalar(95, 47, 63), 1, cv::LINE_AA);
	cv::line(dados->buffer, dados->a, dados->b, cv::Scalar(192, 96, 128));
	cv::arrowedLine(dados->buffer, dados->ponto, dados->projecao, cv::Scalar(63, 191, 255), 1, cv::LINE_8, 0, 0.05);
	cv::rectangle(dados->buffer, dados->a - cv::Point2d(2, 2), dados->a + cv::Point2d(3, 3), cv::Scalar(0, 192, 96), -1, cv::LINE_8, 0);
	cv::rectangle(dados->buffer, dados->b - cv::Point2d(2, 2), dados->b + cv::Point2d(3, 3), cv::Scalar(0, 192, 96), -1, cv::LINE_8, 0);
	cv::rectangle(dados->buffer, dados->projecao - cv::Point2d(2, 2), dados->projecao + cv::Point2d(3, 3), cv::Scalar(96, 31, 255), -1, cv::LINE_8, 0);
	cv::rectangle(dados->buffer, dados->ponto - cv::Point2d(2, 2), dados->ponto + cv::Point2d(3, 3), cv::Scalar(0, 192, 96), -1, cv::LINE_8, 0);
	cv::imshow("Point-Line Projection", dados->buffer);
	//	cv::updateWindow("Point-Line Projection");	//NOTE: No OpenGL overlay, so not needed.
}

void ProjetaPonto(Dados *dados) {
	if(dados->a.x == dados->b.x && dados->a.y == dados->b.y)	{
		dados->projecao = dados->a;
		return;
	}
	else {
		dados->projecao = dados->ponto;
	}
	float crossz = (dados->ponto.x - dados->a.x) * (dados->ponto.y - dados->b.y) - (dados->ponto.y - dados->a.y) * (dados->ponto.x - dados->b.x);
	if (crossz*crossz < 0.01)	{
		return;
	}

	bool flipped = false;	//NOTE: Flip coordinates to avoid infty result when line is perfectly horizontal. Meant to keep code clean instead of coding all math twice.
	if (dados->a.y == dados->b.y) {
		Flip(dados);
		flipped = true;
	}

	float delta = (pow((dados->a.y - dados->b.y), 2.0)*(pow(dados->a.y, 4.0) + ((dados->a.y*dados->a.y)*((2.0*(dados->b.x - dados->a.x)*(dados->ponto.x - dados->a.x)) + (dados->b.y*dados->b.y) + (4.0*dados->b.y*dados->ponto.y) + (dados->ponto.y*dados->ponto.y))) + (2.0*((dados->b.y*dados->ponto.y*(dados->b.x - dados->a.x)*(dados->ponto.x - dados->a.x)) - (dados->a.x*(((dados->b.x*dados->b.x)*dados->ponto.x) + ((dados->ponto.x*dados->ponto.x)*dados->b.x))) - (pow(dados->a.x, 3.0)*(dados->b.x + dados->ponto.x)) + ((dados->b.y + dados->ponto.y)*((dados->a.y*((dados->a.x*(dados->b.x + dados->ponto.x)) - (dados->a.x*dados->a.x) - (dados->ponto.y*dados->b.y) - (dados->ponto.x*dados->b.x))) - pow(dados->a.y, 3.0))))) + pow(dados->a.x, 4.0) + ((dados->a.x*dados->a.x)*((dados->b.x*dados->b.x) + (4.0*dados->b.x*dados->ponto.x) + (dados->ponto.x*dados->ponto.x))) + pow((dados->b.x*dados->ponto.x), 2.0) + pow((dados->b.y*dados->ponto.y), 2.0)));
	if(delta < 0)	{}
	else if (delta < 1) {
		dados->projecao.y = (dados->a.y/2.0) + (((((dados->ponto.y*((dados->b.y*dados->b.y) + (dados->a.y*dados->a.y))) + (dados->b.y*(dados->b.x - dados->a.x)*(dados->ponto.x - dados->a.x)))/2.0) + (dados->a.y*(((dados->a.x - dados->b.x)*(dados->ponto.x - dados->b.x)/2.0) - (dados->b.y*dados->ponto.y))))/((dados->a.x*dados->a.x) - (2.0*((dados->a.x*dados->b.x) + (dados->b.y*dados->a.y))) + (dados->b.x*dados->b.x) + (dados->b.y*dados->b.y) + (dados->a.y*dados->a.y)));
		dados->projecao.x = ((dados->b.x*(dados->projecao.y - dados->a.y)) + (dados->a.x*(dados->b.y - dados->projecao.y)))/(dados->b.y - dados->a.y);
	}
	else {
		float tmp1x, tmp1y;
		dados->projecao.y = ((dados->a.y/2.0) + (((( pow(delta, (1.0/2.0)) + (dados->ponto.y*((dados->b.y*dados->b.y) + (dados->a.y*dados->a.y))) + (dados->b.y*(dados->b.x - dados->a.x)*(dados->ponto.x - dados->a.x)))/2.0) + (dados->a.y*(((dados->a.x - dados->b.x)*(dados->ponto.x - dados->b.x)/2.0) - (dados->b.y*dados->ponto.y))))/((dados->a.x*dados->a.x) - (2.0*((dados->a.x*dados->b.x) + (dados->b.y*dados->a.y))) + (dados->b.x*dados->b.x) + (dados->b.y*dados->b.y) + (dados->a.y*dados->a.y))));
		dados->projecao.x = ((dados->b.x*(dados->projecao.y - dados->a.y)) + (dados->a.x*(dados->b.y - dados->projecao.y)))/(dados->b.y - dados->a.y);
		tmp1x = dados->projecao.x - dados->a.x;
		tmp1y = dados->projecao.y - dados->a.y;
		if(tmp1x*tmp1x + tmp1y*tmp1y < 1)	{
			dados->projecao.y = ((dados->a.y/2.0) + ((((-pow(delta, (1.0/2.0)) + (dados->ponto.y*((dados->b.y*dados->b.y) + (dados->a.y*dados->a.y))) + (dados->b.y*(dados->b.x - dados->a.x)*(dados->ponto.x - dados->a.x)))/2.0) + (dados->a.y*(((dados->a.x - dados->b.x)*(dados->ponto.x - dados->b.x)/2.0) - (dados->b.y*dados->ponto.y))))/((dados->a.x*dados->a.x) - (2.0*((dados->a.x*dados->b.x) + (dados->b.y*dados->a.y))) + (dados->b.x*dados->b.x) + (dados->b.y*dados->b.y) + (dados->a.y*dados->a.y))));
			dados->projecao.x = ((dados->b.x*(dados->projecao.y - dados->a.y)) + (dados->a.x*(dados->b.y - dados->projecao.y)))/(dados->b.y - dados->a.y);
		}
	}

	if(flipped == true) {
		Flip(dados);
	}
	return;
}

void Flip(Dados *dados) {
	Flip(&(dados->a));
	Flip(&(dados->b));
	Flip(&(dados->ponto));
	Flip(&(dados->projecao));
}

void Flip(cv::Point2d *p) {
	int tmp;
	tmp = p->x;
	p->x = p->y;
	p->y = tmp;
}
