TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

TARGET = plp

LIBS +=	-lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
